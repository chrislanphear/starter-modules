<?php
$cols = 12 / $module['columns'];
?>
<div class="row">
  <div class="mod-col space-bottom20 col-sm-<?= $cols; ?>">
    <?= $module['column_1']; ?>
  </div><!-- /.col-sm-<?= $cols; ?> -->

  <?php if ( $module['columns'] > 1 && $module['column_2'] ) : ?>
  <div class="mod-col space-bottom20 col-sm-<?= $cols; ?>">
    <?= $module['column_2']; ?>
  </div><!-- /.col-sm-<?= $cols; ?> -->
  <?php endif; ?>

  <?php if ( $module['columns'] > 2 && $module['column_3'] ) : ?>
  <div class="mod-col space-bottom20 col-sm-<?= $cols; ?>">
    <?= $module['column_3']; ?>
  </div><!-- /.col-sm-<?= $cols; ?> -->
  <?php endif; ?>

  <?php if ( $module['columns'] > 3 && $module['column_4'] ) : ?>
  <div class="mod-col space-bottom20 col-sm-<?= $cols; ?>">
    <?= $module['column_4']; ?>
  </div><!-- /.col-sm-<?= $cols; ?> -->
  <?php endif; ?>
</div><!-- /.row -->
