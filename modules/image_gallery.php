<?php $i = 0; ?>
<div class="row">
  <?php foreach ( $module['images'] as $image ) : $i++; ?>
  <div class="mod-col space-bottom20 col-sm-3">
    <a href="<?= $image['url']; ?>" data-toggle="lightbox" data-gallery="<?= $module_id; ?>" data-parent="<?- $module_id; ?>">
      <div class="overlay-wrap">
        <img src="<?= $image['sizes']['shop_single']; ?>" class="img-responsive center-block">
        <div class="overlay">
          <i class="fa fa-lg fa-search-plus"></i>
        </div>
      </div>
    </a>
  </div><!-- /.col-sm-3 -->

  <?php if ( $i % 4 == 0 ) echo '</div><!-- /.row --><div class="row">'; ?>

  <?php endforeach; ?>
</div>
