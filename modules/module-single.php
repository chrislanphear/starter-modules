<?php
global $module_id;
$module_id++;

unset( $module_class, $background, $bg_color, $bg_color_custom, $bg_image, $module, $color );

$fields_to_get = [
  'background',
  'bg_color',
  'bg_color_custom',
  'bg_image',
  'mod_margin',
  'mod_padding',
  'module',
];

foreach ( $fields_to_get as $field ) {
  ${ $field } = get_sub_field( $field );
}

$module = $module[0];

if ( $i == 1 ) $module_class .= 'module-first';
if ( $i == $count ) $module_class .= ' module-last';

if ( $background != 'transparent' )
  $module_class .= ' bg-' . $bg_color;
?>
<section id="module-<?= $module_id; ?>" class="modules <?= $module['acf_fc_layout']; ?> <?= $module_class; ?>" data-module="<?= $module['acf_fc_layout']; ?>" data-background="<?= $background; ?>" data-bg-color="<?= $bg_color; ?>" data-bg-color-custom="<?= $bg_color_custom; ?>" data-bg-image="<?= $bg_image; ?>">
  <div class="mod-cont">
    <!--
    <?php print_r( $module ); ?>
    -->

    <?php
    if ( locate_template( 'modules/' . $module['acf_fc_layout'] . '.php' ) ) {
      include( get_stylesheet_directory() . '/modules/' . $module['acf_fc_layout'] . '.php' );
    }
    ?>
  </div><!-- /.<?= $container_class; ?> -->
</section><!-- /.modules .<?= $module['acf_fc_layout']; ?> -->

<?php if ( $background == 'color' || $background == 'image' || $background == 'pattern' || isset( $mod_margin ) || isset( $mod_padding ) ) :
  if ( $bg_color == 'primary' ) $color = $brand_primary;
  if ( $bg_color == 'secondary' ) $color = $brand_secondary;
  if ( $bg_color == 'custom' ) $color = $bg_color_custom;
?>
<style type="text/css">
#module-<?= $module_id; ?> {
  <?php if ( $background == 'color' && $bg_color ) : ?>
  background-color: <?= $color; ?>;
  <?php endif; ?>
  <?php if ( $background == 'image' || $background == 'pattern' ) : ?>
  background-image: url('<?= $bg_image; ?>');
  <?php endif; ?>
  <?php if ( $mod_margin ) : ?>
  margin: <?= $mod_margin; ?>;
  <?php endif; ?>
  <?php if ( $mod_padding ) : ?>
  padding: <?= $mod_padding; ?>;
  <?php endif; ?>
}
</style>
<?php endif; ?>
