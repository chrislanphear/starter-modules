<div class="row">
  <div class="<?= $module['featured_item']; ?> space-bottom20 col-sm-<?= $module['item_width']; ?><?= ( $module['item_placement'] == 'right' ) ? ' col-sm-push-' . ( 12 - $module['item_width'] ) : ''; ?>">

    <?php if ( $module['featured_item'] == 'image' ) : ?>
    <img src="<?= $module['image']['url']; ?>" class="img-responsive">
    <?php else : ?>
    <div class="embed-responsive embed-responsive-16by9">
      <?= $module['video']; ?>
    </div>
    <?php endif; ?>
  </div><!-- /.col-sm-<?= $module['item_width']; ?> -->

  <div class="content space-bottom20 col-sm-<?= 12 - $module['item_width']; ?><?= ( $module['item_placement'] == 'right' ) ? ' col-sm-pull-' . $module['item_width'] : ''; ?>">
    <?= $module['content']; ?>
  </div><!-- /.col-sm-<?= 12 - $module['item_width']; ?> -->
</div><!-- /.row -->
